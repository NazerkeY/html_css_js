var request = new XMLHttpRequest();

const app = document.getElementById('root');

// const header = document.createElement('h2');
// header.textContent = "Top Anime List";

const container = document.createElement('div');
container.setAttribute('class', 'container');

// header.id = "header";

// app.appendChild(header);
app.appendChild(container);

request.open('GET', 'https://ghibliapi.herokuapp.com/films', true);

request.onload = function(){
	var data = JSON.parse(this.response);

	if(request.status >=200 && request.status < 400){

		for(var i = 0; i < data.length; i++){
			const card = document.createElement('div');
			card.setAttribute('class', 'card');

			const title = document.createElement('h2');
			title.id = "title"
			title.textContent = data[i].title;

			const description = document.createElement('p');
			data[i].description = data[i].description.substring(0, 300);
			description.textContent = `${data[i].description}...`;

			container.appendChild(card);
			card.appendChild(title);
			card.appendChild(description);
		}
	}
	else{
		const errorMessage = document.createElement('marquee');
		errorMessage.textContent = "Sorry, our site doesn't response right now.";
		app.appendChild(errorMessage);
	}
}

request.send();